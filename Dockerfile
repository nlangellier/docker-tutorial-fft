# docker tutorial Dockerfile

# "FROM" is to Dockerfiles as "import" is to Python
# Use Ubuntu 18.04 LTS as base OS
FROM ubuntu:18.04

# "LABEL" adds metadata that can be viewed with "docker inspect"
# Label myself as the author of this Dockerfile
LABEL maintainer="Nick Langellier <nlangellier@gmail.com>"

# "RUN" executes multiple shell commands separated by "&&"
# Install version specific software
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        software-properties-common=0.96.24.32.3 \
        build-essential=12.4ubuntu1 \
        git=1:2.17.1-1ubuntu0.1 \
        python3=3.6.5-3ubuntu1 \
        python3-dev=3.6.5-3ubuntu1 \
        python3-pip=9.0.1-2.3~ubuntu1 \
        python3-setuptools=39.0.1-2 \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Update pip
RUN python3 -m pip install pip==10.0.1

# Install version specific Python libraries
RUN pip --no-cache-dir install \
        ipykernel==4.8.2 \
        jupyter==1.0.0 \
        matplotlib==2.2.2 \
        numpy==1.14.5 \
        scipy==1.1.0 \
        && \
    python3.6 -m ipykernel.kernelspec

# "ARG" is a variable users can pass at build time
# Set gitlab username and repository name
ARG user
ARG repo

# "ENV" sets an environment variable
# Create an environment variable with the gitlab repo URL
ENV GITLAB_URL=https://gitlab.com/$user/$repo.git

# Set working directory
WORKDIR /home/

# Clone repository into working directory
RUN git clone $GITLAB_URL

# Run Jupyter notebook
CMD ["jupyter", "notebook", "--ip=0.0.0.0", "--no-browser", "--allow-root"]
